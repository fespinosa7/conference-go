from django.http import JsonResponse
from .models import Attendee, ConferenceVO
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
    ]

    encoders = {
    "conference": ConferenceVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):

    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

    try:
        # THIS LINE IS ADDED
        conference_href = f'/api/conferences/{conference_vo_id}/'

        # THIS LINE CHANGES TO ConferenceVO and import_href
        conference = ConferenceVO.objects.get(import_href=conference_href)

        content["conference"] = conference

           ## THIS CHANGES TO ConferenceVO
    except ConferenceVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid conference id"},
            status=400,
        )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            {"attendee": attendee},
            encoder=AttendeeListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(conference_id=id)
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse({"message": "Invalid conference id"}, status=400)
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
