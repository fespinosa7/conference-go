from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}"
        }
    url = "https://api.pexels.com/v1/search"

    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"]["0"]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def lat_long(city, state):
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    request = requests.get(url, params=params)
    content = json.loads(request.content)
    return (content[0]["lat"], content[0]["lon"])


def get_weather_data(city, state):
    try:
        coords = lat_long(city, state)
    except:
        return {"weather": None}
    params = {
        "lat": coords[0],
        "lon": coords[1],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    request = requests.get(url, params=params)
    content = json.loads(request.content)
    try:
        weather = {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
    except:
        weather = {"weather": None}
    return weather
